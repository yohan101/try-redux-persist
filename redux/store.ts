import { combineReducers, createStore } from "redux";
import { persistStore, persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage"; // defaults to localStorage for web
import { countReducer } from "./test/reducer";

const persistConfig = {
  key: "root",
  storage,
};

const rootReducer = combineReducers({ count: countReducer });
const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = createStore(persistedReducer);
export const persistor = persistStore(store);

export type ReduxState = {
  count: number;
};
