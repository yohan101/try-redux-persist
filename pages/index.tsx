import type { NextPage } from "next";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";

import { persistor, store } from "../redux/store";

import AccessReduxComponent from "../components/AccessReduxComponent";

const Home: NextPage = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AccessReduxComponent />
      </PersistGate>
    </Provider>
  );
};

export default Home;
