import { useDispatch, useSelector } from "react-redux";
import { ReduxState } from "../redux/store";

function AccessReduxComponent() {
  const dispatch = useDispatch();
  const count = useSelector((state: ReduxState) => state.count);

  return (
    <div>
      <button onClick={() => dispatch({ type: "DECREMENT" })}>-</button>
      <p>{count}</p>
      <button onClick={() => dispatch({ type: "INCREMENT" })}>+</button>
    </div>
  );
}

export default AccessReduxComponent;
